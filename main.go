package main 

import (
    "database/sql"
    "log"
    "net/http"
    "encoding/json"
    "github.com/gorilla/mux"
    _ "github.com/go-sql-driver/mysql"
    "strconv"
)

type User struct{
	Id int
	Nombre string
	Passwd string
	AccessLevel int
}

func dbConnection()( db *sql.DB){
	dbDriver := "mysql"
	dbUser := "root"
	dbPasswd := ""
	dbName := "test"
	db, err:= sql.Open(dbDriver, dbUser+":"+dbPasswd+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	return db

}

func All(w http.ResponseWriter, req *http.Request){

	db := dbConnection()
	selDB, err := db.Query("SELECT * FROM user") 
	if err != nil{
		panic(err.Error())
	}

	u := User{}
	users := []User{}

	for selDB.Next(){
		var id, accessLevel int
		var nombre, passwd string
		err = selDB.Scan(&id, &nombre, &passwd, &accessLevel)
		if err != nil{
			panic(err.Error())
		}
		u.Id = id
		u.Nombre = nombre
		u.Passwd = passwd
		u.AccessLevel = accessLevel

		users = append(users, u)

	}

	json.NewEncoder(w).Encode(users)
	defer db.Close()
	}
func One(w http.ResponseWriter, req *http.Request){

	db := dbConnection()

	params := mux.Vars(req)
	selDB, err := db.Query("SELECT * FROM user where id = ?", params["id"]) 
	if err != nil{
		panic(err.Error())
	}

	u := User{}
	users := []User{}

	for selDB.Next(){
		var id, accessLevel int
		var nombre, passwd string
		err = selDB.Scan(&id, &nombre, &passwd, &accessLevel)
		if err != nil{
			panic(err.Error())
		}
		u.Id = id
		u.Nombre = nombre
		u.Passwd = passwd
		u.AccessLevel = accessLevel

		users = append(users, u)

	}

	json.NewEncoder(w).Encode(users)
	defer db.Close()
	}
func Insert(w http.ResponseWriter, req *http.Request) {
    db := dbConnection()
    
    var user User
    _ = json.NewDecoder(req.Body).Decode(&user)
    
    query, err := db.Prepare("INSERT INTO user(nombre, passwd, accessLevel) VALUES(?,?,?)")
    if err != nil {
        panic(err.Error())
    }
    query.Exec(user.Nombre, user.Passwd, user.AccessLevel)
    log.Println("INSERT: Nombre: " + user.Nombre + " | Passwd: " + user.Passwd + "| accessLevel: "+ strconv.Itoa(user.AccessLevel))
    
    defer db.Close()

    resp := `{"mgs": "INSERTADO", "cod": 200}`
    json.NewEncoder(w).Encode(resp)
	}
func Update(w http.ResponseWriter, req *http.Request) {
    db := dbConnection()
    params := mux.Vars(req)
    
    var user User
    _ = json.NewDecoder(req.Body).Decode(&user)
    
    query, err := db.Prepare("UPDATE user set nombre =? , passwd = ? , accessLevel =? where id =" + params["id"])
    if err != nil {
        panic(err.Error())
    }
    query.Exec(user.Nombre, user.Passwd, user.AccessLevel)
    log.Println("UPDATE: Nombre: " + user.Nombre + " | Passwd: " + user.Passwd + "| accessLevel: "+ strconv.Itoa(user.AccessLevel))
    
    defer db.Close()

    resp := `{"mgs": "MODIFICADO", "cod": 200}`
    json.NewEncoder(w).Encode(resp)
	}
func Delete(w http.ResponseWriter, req *http.Request) {
    db := dbConnection()
    params := mux.Vars(req)
    
    
    query, err := db.Prepare("DELETE from user where id =" + params["id"])
    if err != nil {
        panic(err.Error())
    }
    query.Exec()

    log.Println("BORRADO")
    
    defer db.Close()

    resp := `{"mgs": "BORRADO", "cod": 200}`
    json.NewEncoder(w).Encode(resp)
	}

func main(){
	router := mux.NewRouter()

	log.Println("Conectado")
	router.HandleFunc("/users", All).Methods("GET")
	router.HandleFunc("/users", Insert).Methods("POST")
	router.HandleFunc("/users/{id}", One).Methods("GET")
	router.HandleFunc("/users/{id}", Update).Methods("PUT")
	router.HandleFunc("/users/{id}", Delete).Methods("DELETE")
	http.ListenAndServe(":8080", router)
}